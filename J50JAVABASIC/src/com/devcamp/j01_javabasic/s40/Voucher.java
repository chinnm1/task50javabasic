package com.devcamp.j01_javabasic.s40;

public class Voucher {
    public String voucherCode = "V000000";

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;

    }

    public void showVoucher() {
        System.out.println("Vouchercode is" + this.voucherCode);

    }

    public void showVoucher(String voucherCode) {
        System.out.println("this is voucher code" + this.voucherCode);
        System.out.println("This is voucher code too" + voucherCode);

    }

    public static void main(String[] args) {
        Voucher voucher = new Voucher();
        // 0 Chạy class này rồi comment dòng code dưới
        voucher.showVoucher();
        // 1.Bỏ comment dòng code dưới rồi chạy lại class này
        // voucher.showVoucher("CODE8888");
        /*
         * 2. comment lại dòng code trên
         * bỏ comment 2 dòng code dưới rồi chạy class này
         */
        // voucher.voucherCode = "CODE2222";
        // voucher.showVoucher("0008888");
        /*
         * 3. Comment 2 dòng code trên
         * Bỏ comment các dòng class dưới và chạy code này
         */
        String code = "VOUCHER";
        voucher.setVoucherCode(code);
        System.out.println(voucher.getVoucherCode());
        voucher.showVoucher(voucher.getVoucherCode());
        voucher.showVoucher();

    }

    // public static void showVoucher(String voucherCode) {
    // System.out.println("This is voucher code" + voucherCode);

    // }
}
