import com.devcamp.j50_javabasic.s10.NewDevcampApp;

public class App {
    public static void main(String[] args) throws Exception {
        // This is a commetn
        String appName = "Devcamp will help everyone to know coding";
        /*
         * This is comment for multi line
         * line 1
         * lin2
         */
        System.out.println("Hello, World!" + appName.length());
        System.out.println("uppercase" + appName.toUpperCase());
        System.out.println("LOWERCASE" + appName.toLowerCase());
        NewDevcampApp.name("HieuHN", 42);
        NewDevcampApp newApp = new NewDevcampApp();
        newApp.name("Ha Ngoc Hieu");
    }
}
